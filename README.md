# NLP scripts (corpura formatting) #

Python scripts for manipulation of EXB and TMX files. These 3 scripts were created for a friend for his PhD project. It was related to transcriptions of interviews made with the software EXMARaLDA, that saves in .exb format, and the alignment of text with the software YouAlign. The exported files were not compatible between both software�s, so these scrips were made to mediate that conversion.

Summarizing: one script converts from many HTML files to a single one (using a specific structure needed for the software); other converts from EXB format to a specific HTML structure (able to be imported in YouAlign in one go: the free version of the software limits the number of files per day, so this way with one HTML file one is able to import multiple texts in one go); the 3rd script manipulates the resulting files from the software YouAlign (in TMX format) to a specific format needed for the project, converting back to individual files in the appropriate format.

I share this in case it is useful to someone working with these tools for Linguistics/Copora: open-source license.

## Contacts:
My Homepage: [www.paulojorgepm.net](http://www.paulojorgepm.net)