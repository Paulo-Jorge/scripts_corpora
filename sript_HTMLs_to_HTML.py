#!/usr/bin/env python
# -*- coding: utf-8 -*-

print(u"""
====================================================
====================================================
Data: 02/2015
E-mail: paulo.jorge.pm@gmail.com
Tested on: Windows 8, Python 2.7

Função deste scritp: converte vários ficheiros .html ou .htm gerados pelo programa EXMARaLDA Partitur-Editor para um único HTML limpo, onde o conteúdo de cada ficheiro original é uma tabela: o 1º <td> contém o código da tradução (p.e. 13CN7EN); o 2º <td> contém o texto do discurso.

Como utilizar: todos os ficheiros a converter devem estar numa raiz de diretório comum. Colar este script na raiz e correr: ele percorrerá a raiz e todos os diretórios e subdiretórios em busca de ficheiros com terminação .html ou .htm, copiando e limpando os conteúdos de cada um (devem ter uma estrutura HTML típica do EXMARaLDA). Nota: nunca destrói nem altera os ficheiros originais, apenas lê.
No final do processo é gerado um pequeno relatório na consola com o nome dos ficheiros percorridos, número total de ficheiros processados e de erros. Para confirmar o utilizador deverá carregar na tecla enter e os dados serão guardados numa pasta chamada "save" relativa ao diretório do script, contendo a versão PT.html e EN.html dos dados.

Extra opcional: 
1) Padroniza, adicionando 3 asteriscos ao início e fim de cada segmento quando não os encontra.
2) Padroniza, adicionando um espaço entre os 3 asteriscos iniciais e finais e o texto, quando não tem
3) Limpa todos os espaços antes e depois do bloco de texto
====================================================
====================================================
""")

import os
import sys
import codecs
import lxml.html

counter = 0
counter_pt = 0
counter_en = 0
file_errors = 0
file_errors_name = ''
html_errors = 0
txt_pt = ""
txt_en = ""
IDs_names_pt = []
IDs_names_en = []

print(u"\nA processar ficheiros:\n")
print(u"\nExtraindo ID e organizando por língua cada ficheiro:\n")

for root, dirs, files in os.walk("."):
    for file in files:
        if file.endswith((".html", ".htm")):
            try:
                file_open = codecs.open(file, "rb", "utf-8")
                file_text =  file_open.read().encode("utf-8")
                file_open.close

                counter = counter + 1
                
                dom =  lxml.html.fromstring(file_text)
                for value in dom.xpath("//table[1]/tr[1]/td[1]/span[@class='ROW-LABEL']"):
                    name = value.text
                    print(str(counter) + ": " + file_open.name + " -> " + name)
                for value in dom.xpath("//table[1]/tr[1]/td[2]/*"):
                    text = value.text
                
                #limpeza e padronizar
                name = name.strip()
                text = text.strip()
                #padronizar os 3 asteriscos iniciais e finais - acrescentar quando não aparecem
                if text[0] != "*":
                    text = "*** " + text
                if text[-1] != "*":
                    text = text + " ***"
                #acrescentar um espaço entre o texto e os 3 asteriscos iniciais e finais se não houver
                if text[3] != " ":
                    text = text[:3] + " " + text[3:]
                if text[-3] != " ":
                        text = text[:-3] + " " + text[-3:]
                
                segment = "\n<table><tr><td>" + name + "</td><td>" + text + "</td></tr></table>\n"
                
                if name[-2:] == "PT":
                    txt_pt = txt_pt + segment
                    IDs_names_pt.append(name)
                    counter_pt = counter_pt + 1
                elif name[-2:] == "EN":
                    txt_en = txt_en + segment
                    IDs_names_en.append(name)
                    counter_en = counter_en + 1
                else:
                    html_errors = html_errors + 1
                
            except:
                file_errors = file_errors + 1
                file_errors_name = file_errors_name + file + ' | '
                
print("\n")
print("====================================")
print("====================================")
print("Ficheiros processados: " + str(counter))
print("Discursos PT encontrados: " + str(counter_pt))
print("Discursos EN encontrados: " + str(counter_en))
print("Ficheiros que geraram erro ao abrir (total " + str(file_errors) + "): " + file_errors_name)
print("Ficheiros que geraram erro ao processar o HTML: " + str(html_errors))
print("====================================")
print("====================================")
print("\n")


print("Data will be saved to:")
print(os.path.join(os.path.dirname(os.path.abspath(__file__)), "save" ,"pt.html"))
print(os.path.join(os.path.dirname(os.path.abspath(__file__)), "save", "en.html"))
print(os.path.join(os.path.dirname(os.path.abspath(__file__)), "save", "ids.py"))
print("\n")

raw_input("Press any key to confirm and save...")

#save
def save(file_name,text):
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "save", file_name)
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    file = codecs.open(path, "w", encoding='utf8')
    file.write(text)
    file.close()

save("en.html", '<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head>\n<body>\n' + txt_en + '\n</body>\n</html>')
save("pt.html", '<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head>\n<body>\n' + txt_pt + '\n</body>\n</html>')


#log de ids e erros
en_alone = ''
pt_alone = ''
for id in IDs_names_pt:
    id_en = id[:-2] + 'EN'
    if id_en not in IDs_names_en:
        pt_alone = pt_alone + id + '\n'
        
for id in IDs_names_en:
    id_pt = id[:-2] + 'PT'
    if id_pt not in IDs_names_pt:
        en_alone = en_alone + id + '\n'

ids = """#!/usr/bin/env python
# -*- coding: utf-8 -*-

ids_pt = """ + repr(IDs_names_pt) + """

ids_en = """ + repr(IDs_names_en) + """

------------------------------------------
Ficheiros EN sem par: """ + en_alone + """

Ficheiros PT sem par: """ + pt_alone

save("ids.py",ids)