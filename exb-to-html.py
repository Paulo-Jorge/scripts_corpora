#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import codecs
import lxml.etree

ids = []
ids_path = {}
file_count = 0
text_errors = ''
save_errors = ''
repeated_error = ''

def save(file_name,text):
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "save", file_name)
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    file = codecs.open(path, "w", encoding='utf8')
    file.write(text)
    file.close()

for root, dirs, files in os.walk("."):
    for file in files:
        if file.lower().endswith('.exb'):
            file_path = os.path.join(root,file)
            
            file_open = codecs.open(file_path, "rb", encoding="utf-8")
            file_text =  file_open.read().encode("utf-8")
            file_open.close
            
            print 'errrrrooooooooooo' +file_open.name
            dom =  lxml.etree.fromstring(file_text)
            
            id = ''
            text = ''
            p = ''
            
            print ('\n\n\nFile:' + file_open.name + '=====')
            
            for id in dom.xpath("//abbreviation"):
                id = id.text.strip()
                if id not in ids:
                    ids.append(id)
                    ids_path[id] = file_open.name
                else:
                    repeated_error = repeated_error + 'ID: ' + id + ' | File name: ' + file_open.name + '\n'
                file_count = file_count + 1
                print('[' + str(file_count) + ']\n===== ID:' + id)
                
            for p in dom.xpath("//tier/event"):
                if p.text:
                    text = text + p.text
                    print p.text.encode("utf-8")
                else:
                    text_errors = text_errors + file_open.name + '\n'
                
            try:
                save(id+".html", '<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head>\n<body>\n<table><tr><td><span class="ROW-LABEL">' + id + '</span></td><td><span>' + text + '</span></td></tr></table>\n</body>\n</html>')
            except:
                save_errors = save_errors + file_open.name + '\n'


print("\n")
print("====================================")
print("====================================")
print("Ficheiros processados: " + str(file_count))
print("Erros no texto dos ficheiros: " + text_errors)
print("Erros a gravar os ficheiros: " + save_errors)
print("Ficheiros com IDs repetidos: " + repeated_error)
print("====================================")
print("====================================")
print("\n")

try:
    txt_ids = ''
    for id in ids:
        txt_ids = txt_ids + 'ID: ' + id + ' | ' + 'Origem: ' + ids_path[id] + '\n'
    txt = "Individual IDs (" + str(len(ids)) + " total):\n" + txt_ids + "\n\n" + "Total de ficheiros processados: " + str(file_count) + "\n\n" "Erros no texto dos ficheiros:\n" + text_errors + "\n\n" + "Erros a gravar os ficheiros:\n" + save_errors + "\n\n" + "Ficheiros com IDs repetidos:\n" + repeated_error
    

    
    log_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "log.txt")
    file = open(log_path, "w")
    file.write(txt)
    file.close()

except:
    print('ERRO A GUARDAR/CRIAR UM LOG DOS ERROS')

print("Data saved to:" + os.path.join(os.path.dirname(os.path.abspath(__file__)), "save"))
print("Log/errors saved to:" + log_path)

print("\n")

raw_input("...done...")