#!/usr/bin/env python
# -*- coding: utf-8 -*-

print(u"""
====================================================
====================================================
Data: 02/2015
E-mail: paulo.jorge.pm@gmail.com
Tested on: Windows 8, Python 2.7 (+ lxml)

Como funciona: colocar no mesmo diretório do TMX gerado pelo YouAlign já corrigido e bem estruturado (Nota 1: não deve haver mais do que um TMX no diretório, o script procura automaticamente por todos os TMX no mesmo diretório, mas foi testado apenas com um TMX em mente, pode gerar erros se encontrar mais do que um; Nota 2: assume que o TMX tem encoding utf-16, será o encoding de leitura e gravação). Este script navega por todos as tags <tu> à procura de 3 asteriscos no início e fim de cada segmento, inserindo as tags <speech_segment> antes do <tu> respetivo, fazendo o mesmo quando encontra um <tu> com apenas campos de ID, do tipo "10CS9PT" (p.e.) com terminação PT ou EN e sem espaços, adicionado <speech_ID> antes do <tu> respetivo. Cada segmento é dividido por <speech_unit>x</speech_unit>, com a seguinte estrutura por segmento:
<speech_unit>
<speech_ID><tu>xxx</tu></speech_ID>
<speech_segment><tu>xxx</tu><tu>etc...</tu></speech_segment>
</speech_unit>

O TMX final é gravado na pasta "save_tmx" relativa ao diretório do scritp, com o nome "final.tmx" e encoding utf-16
====================================================
====================================================
""")

import os
import sys
import codecs
import lxml.etree

tmx_counter = 0
tu_counter = 0
tmx = ""
file_errors = 0
ids_count = 0
segments_start_count = 0
segments_en_count = 0

print(u"\nA processar o ficheiro TMX:\n")

files = [f for f in os.listdir('.') if os.path.isfile(f)]
for file in files:
    if file.lower().endswith(".tmx"):
     
        file_open = codecs.open(file, "rb", encoding="utf-16")
        file_text =  file_open.read().encode("utf-16")
        file_open.close
        
        tmx_counter = tmx_counter + 1
        
        dom =  lxml.etree.fromstring(file_text)
        for tu in dom.xpath("//tu"):

            #se for 1º bloco de ID de um novo segmento
            if " " not in tu[0][0].text.strip() and (tu[0][0].text[-2:] == "PT" or tu[0][0].text[-2:] == "EN"):
                tmx = tmx + "<speech_unit>\n<speech_ID>\n" + lxml.etree.tostring(tu, encoding='utf-16', xml_declaration=False).decode("utf-16") + "</speech_ID>\n"
                ids_count = ids_count + 1
                print "ID" + str(ids_count) + ": <speech_unit><speech_ID>" + tu[0][0].text + "..."
                
            #se for 1º bloco de texto do segmento    
            elif tu[0][0].text.strip().startswith("***"):
                tmx = tmx + "<speech_segment>\n" + lxml.etree.tostring(tu, encoding='utf-16', xml_declaration=False).decode("utf-16")
                segments_start_count = segments_start_count + 1
                print "SEG" + str(segments_start_count) + ": <speech_segment>" + tu[0][0].text[:50] + "..."
            
            #se for o último bloco de texto do segmento
            elif tu[0][0].text.strip().endswith("***"):
                tmx = tmx + lxml.etree.tostring(tu, encoding='utf-16', xml_declaration=False).decode("utf-16") + "</speech_segment>\n</speech_unit>\n"
                segments_en_count = segments_en_count + 1

            else:
                tmx = tmx + lxml.etree.tostring(tu, encoding='utf-16', xml_declaration=False).decode("utf-16")
            
            tu_counter = tu_counter + 1

#limpa linha em branco do fim
tmx = tmx.strip()

#adiciona os códigos originais antes e depois do bloco de <tu>'s
tmx = file_text.decode("utf-16").split("<tu>")[0] + tmx + file_text.decode("utf-16").split("</tu>")[-1]

print("\n")
print("====================================")
print("====================================")
print(u"Ficheiros TMX encontrados (deveria ser apenas um): " + str(tmx_counter))
print(u"Tag's <tu> encontradas: " + str(tu_counter))
print(u"Ficheiros que geraram erro ao abrir: " + str(file_errors))
print("\n")
print(u"Tags <speech_unit> e <speech_ID> criadas : " + str(ids_count))
print(u"Tags <speech_segment> criadas : " + str(segments_start_count))
print(u"Tags </speech_segment> e </speech_unit> criadas : " + str(segments_en_count))
print(u"*Se não coincidirem algo está errado na estrutura")
print("====================================")
print("====================================")
print("\n")

raw_input("Press any key to confirm and save...")


#save
def save(file_name, text):
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "save_tmx", file_name)
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    file = codecs.open(path, "w", "utf-16")
    file.write(text)
    file.close()

try:
    save("final.tmx", tmx)
    print("Done!")
except:
    print("Error!")